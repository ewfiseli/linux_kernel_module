/* 
	Assign 2: Question 2
	CPSC 457 W13
	Author: Eric Fiselier
	SUID: 10087750
	
	This program takes one param "pid", the process to show the maps
	and mimics cat /proc/pid/maps as closely as possible
	
*/
#undef __KERNEL__
#define __KERNEL__

#undef MODULE
#define MODULE

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/types.h> //Provides pid_t
#include <linux/pid.h> // Provides pid_task and find_vpid
#include <linux/sched.h> //Provides task_struct
#include <linux/fs.h> //Provides file and inode structs and methods
#include <linux/dcache.h> //Provides dentry to string method
#include <linux/mm.h> //provides prot_t definitions
#include <linux/string.h> // provides strcpy 
#include <linux/slab.h> // kmalloc, kfree


#define MAX_PATH_SIZE 128


/* Holds all info for printing one memory region */
struct minfo {
	char perms[5];
	char path[MAX_PATH_SIZE];
	unsigned long start, end, offset, inode;
	unsigned int maj_dev, min_dev;
};

/* pid_t is of type int */
/* pid_t -> __kernel_pid_t -> int */
static int pid=0;
module_param(pid, int, 0000);
MODULE_PARM_DESC(pid, "The Processes PID to view");


/* Processes a vma if it is not backed by a file 
   stores info in "info"
   mm_struct is needed to mark stack and heap sections
*/
static void 
process_mem(struct mm_struct *mm, struct minfo *info)
{
	/* zero unused in info */
	info->offset = 0;
	info->maj_dev = 0;
	info->min_dev = 0;
	info->inode = 0;
	/* path can be special case of heap, stack or (if i get working) vdso, vsyscall */
	if (info->start <= mm->start_brk && info->end > mm->start_brk) {
		strcpy(info->path, "[heap]");
	} 
	else if (info->start <= mm->start_stack && info->end > mm->start_stack) {
		strcpy(info->path, "[stack]");
	}
	/* this vma is never in the map 
	else if (info->start == 0xffffffffff600000)) {
		strcpy(info->path, "[vsyscall]");
		
	} */
	else {
		strcpy(info->path, "");
	}
}

/* Process the permission flags of a vma 
   Ouput as user-readable string in perms
*/
static void
process_perms(struct vm_area_struct *vma, char perms[5])
{
	unsigned long vm_flags = vma->vm_flags;
	strcpy(perms, "---p");
	if (vm_flags & VM_READ) perms[0] = 'r';
	if (vm_flags & VM_WRITE) perms[1] = 'w';
	if (vm_flags & VM_EXEC) perms[2] = 'x';
	if (vm_flags & VM_SHARED) perms[3] = 's';
}

/* Process the vma if it has a backing file
   
*/
static void
process_file(struct vm_area_struct *vma, struct minfo *info)
{
	struct file *file = vma->vm_file;
	struct inode *inode = vma->vm_file->f_mapping->host;
	char *tmp, *ntmp;
	info->offset = vma->vm_pgoff * PAGE_SIZE;
	/* d_path does not return original pointer, and I would like to free the memory */
	/* <rant> Any function that does not return the original pointer is annoying
	          Will kfree(ptr) correctly free the modified pointer? I highly doubt it.
	   </rant>
	*/
	tmp = (char*) kmalloc(sizeof(char)*MAX_PATH_SIZE, GFP_KERNEL);
	ntmp = d_path(&file->f_path,  tmp, MAX_PATH_SIZE);
	strcpy(info->path, ntmp);
	kfree(tmp);
	/* set info from inode and inode->superblock->dev_t */
	info->inode = inode->i_ino;
	info->maj_dev = MAJOR(inode->i_sb->s_dev); // Major device ID
	info->min_dev = MINOR(inode->i_sb->s_dev); // Minor device ID
}

/* __init --> main */
static int __init vmalist_init(void)
{
	struct task_struct *ts;
	struct mm_struct *mm;
	struct vm_area_struct *vmap;
	struct minfo info;
	int i;
	printk(KERN_INFO "Initalizing vmalist with pid: %d \n", pid);

	/* Get task struct and verify not null */
	ts = pid_task(find_vpid((pid_t)pid), PIDTYPE_PID);
	if (ts == NULL) {
		printk(KERN_ERR "PID: %d not found\n", pid);
		goto vmalist_end;
	}

	/* Critical Section */
	printk(KERN_DEBUG "Waiting on readlock for pid: %d\n", pid);	
	down_read(&ts->mm->mmap_sem);
	
	/* Loop over every vma, and print it */
	mm = ts->mm;
	vmap = mm->mmap;
	for (i=0; i < mm->map_count; ++i) {
		/* set start and end addresses */
		info.start = vmap->vm_start;
		info.end = vmap->vm_end;
		/* get permissions string */
		process_perms(vmap, info.perms);
		/* if vm_file != null, then print information about backing file */
		if (vmap->vm_file != NULL) {
			process_file(vmap, &info);
		/* otherwise, process as "straight" memory */
		} else {
			process_mem(mm, &info);
		}
		/* All info needed is stored in info */
		printk(KERN_INFO "%lx-%lx %s %08lx %02x:%02x %lu\t%s\n", info.start, info.end, info.perms,
			 					info.offset, info.maj_dev, info.min_dev, info.inode, info.path);
		/* move vmap to next vma */
		vmap = vmap->vm_next;
	}
	printk(KERN_DEBUG "Releasing lock on pid: %d\n", pid);
	up_read(&ts->mm->mmap_sem);
vmalist_end:
	return 0;
}

static void __exit vmalist_cleanup(void)
{
	printk(KERN_INFO "Cleaning up vmalist module\n");
}

module_init(vmalist_init);
module_exit(vmalist_cleanup);
MODULE_LICENSE("GPL"); // Take advantage of it! Who doesn't want a worse 'cat'!
MODULE_AUTHOR("Eric Fiselier");
