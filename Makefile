obj-m := vmalist.o
KDIR := /lib/modules/$(shell uname -r)/build
FLAGS := -Wall
PWD := $(shell pwd)

all:
	$(MAKE) $(FLAGS) -C $(KDIR) M=$(PWD) modules

clean:
	$(MAKE) $(FLAGS) -C $(KDIR) M=$(PWD) clean

